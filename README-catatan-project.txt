
2022-03-15:
Isi form nya Alamat.
Propinsi, misal Jakarta pas ketik J muncul pilihan semua propinsi awal J
Trus kalo pilih Jakarta, dibawahnya muncul dropdown pilihan jakut jakpus dll
Trus abis pilih jakut, dibawahnya muncul kelurahan, dst sampe kodepos
Trs bs save, export ke csv, sql
Database awal kita jgn ambil dr free download takutnya ga akurat, jd ambil dr api resmi
Iya propinsi, kotamadya, kelurahan, kecamatan, kode pos
[5:47 PM, 3/15/2022] Fajar Sulaksono: ini bener export sql ya ?
[5:47 PM, 3/15/2022] fjr-oedadesign: Kl  di save misal hanya jakarta, maka akan kesave seluruh data jakarta
[5:47 PM, 3/15/2022] Fajar Sulaksono: untuk api yg diminta mau diambil dari mana ya ?
[5:48 PM, 3/15/2022] fjr-oedadesign: Export ke file umum, biasanya apa aja ya?
[5:48 PM, 3/15/2022] Fajar Sulaksono: ini exportnya di backend ya ?
[5:48 PM, 3/15/2022] fjr-oedadesign: Nah ino blm tau, data yg akurat ambil dr mana kira2
[5:49 PM, 3/15/2022] fjr-oedadesign: Pas di save bs skalian download ga?
[5:50 PM, 3/15/2022] Fajar Sulaksono: untuk provinsi,kota begini saya belum pernah pake API mas, biasanya pake SQL biasa saja, ambil yg terbaru, karena jarang update jg
[5:50 PM, 3/15/2022] fjr-oedadesign: Mungkin abis save, trus br download
[5:51 PM, 3/15/2022] fjr-oedadesign: Nah sumber yg terakurat dr mana kira2? Sy ada link download, tp gatau kualitasnya
[5:51 PM, 3/15/2022] fjr-oedadesign: https://github.com/ArrayAccess/Indonesia-Postal-And-Area/tree/master/data/csv/62
[5:51 PM, 3/15/2022] fjr-oedadesign: https://github.com/edwardsamuel/Wilayah-Administratif-Indonesia
[5:51 PM, 3/15/2022] fjr-oedadesign: https://github.com/edwardsamuel/Wilayah-Administratif-Indonesia/blob/master/mysql/indonesia.sql
[5:52 PM, 3/15/2022] fjr-oedadesign: Umumnya gmn Jar, misal marketplace besar, apa dia kira2 ambil dr api?
[5:52 PM, 3/15/2022] Fajar Sulaksono: ya kalau mau sql spt ini nanti bisa dicari sumbernya, yg tanggal updatenya terbaru
[5:53 PM, 3/15/2022] Fajar Sulaksono: kalau marketplace ga tau saya mas, tapi setau saya tidak ada api resmi dari pemerintah untuk ini
[5:54 PM, 3/15/2022] fjr-oedadesign: Ok, jd demo ini ga pake nembak api, database yg kita dpt dr api, simpan dulu ke lokal
[5:54 PM, 3/15/2022] fjr-oedadesign: Jd gaboleh konek lsg ke api
[5:54 PM, 3/15/2022] fjr-oedadesign: Cuma kita cari data akurat/latest dr api, trs disimpan dulu ke lokal
[5:54 PM, 3/15/2022] Fajar Sulaksono: database provinsi,kota dari sumber github yg terbaru mas bukan dari API
[5:55 PM, 3/15/2022] Fajar Sulaksono: ini maksudnya gmn ya mas
[5:55 PM, 3/15/2022] fjr-oedadesign: Oo, sumber nya terpercaya?
[5:59 PM, 3/15/2022] fjr-oedadesign: Maksudnya sy pikir bs narik dr api sumber. Tp ternyata ga ada ya?
[5:59 PM, 3/15/2022] Fajar Sulaksono: iya mas yg resmi dari pemerintah setau saya gak ada
[5:59 PM, 3/15/2022] fjr-oedadesign: Kita ba cek ga, misal tokopedia ambil dr mana
[6:00 PM, 3/15/2022] Fajar Sulaksono: tdk bisa mas
[6:00 PM, 3/15/2022] fjr-oedadesign: Hmm gmn ya kira2 dpt source yg bener?
[6:03 PM, 3/15/2022] fjr-oedadesign: Situs pemerintah ga ada yg nyediain ya? Misal pos indonesia?
[6:03 PM, 3/15/2022] Fajar Sulaksono: dari kemendagri ada mas format pdf, tapi tidak ada kodeposnya
[6:03 PM, 3/15/2022] Fajar Sulaksono: https://www.kemendagri.go.id/files/2020/PMDN%2072%20TH%202019+lampiran.pdf
[6:04 PM, 3/15/2022] fjr-oedadesign: https://www.eplusgo.com/daftar-api-lokal-indonesia/
[6:05 PM, 3/15/2022] fjr-oedadesign: https://rajaongkir.com/dokumentasi
[6:06 PM, 3/15/2022] Fajar Sulaksono: ini maksudnya gmna ya mas, itu form inputnya mau buat cek ongkir paket ?
[6:08 PM, 3/15/2022] fjr-oedadesign: Bukan Jar, sy kira2 aja mungkin bs jd sumber terpercaya krn kurir kan punya data alamat Indonesia
[6:09 PM, 3/15/2022] fjr-oedadesign: Td sy cb gugling
[6:10 PM, 3/15/2022] Fajar Sulaksono: setau saya gak mas. rajaongkir hanya keluarin api ongkir saja, bukan api untuk alamat
[6:24 PM, 3/15/2022] fjr-oedadesign: Sy td liat2 disini Jar, ada beberapa link api
[6:24 PM, 3/15/2022] fjr-oedadesign: Oh gt, mungkin website kurir kali ya yg ada alamat
[8:21 PM, 3/15/2022] fjr-oedadesign: Jd gmn Jar, kira2 brp cost nya
[1:19 PM, 3/16/2022] fjr-oedadesign: Sy hrs ngabarin
[1:23 PM, 3/16/2022] Fajar Sulaksono: Untuk costnya, dengan asumsi lokasinya tanpa API, bisa saya bantu costnya 500rb ya mas, dengan spesifikasi 1 halaman frontend & halaman backend fleksibel sesuai dgn yg dibutuhkan, teknologi pake framework PHP Laravel
[1:25 PM, 3/16/2022] Fajar Sulaksono: yg eplusgo api terkait lokasi ada 3 ini, yg ibachor itu hanya mencakup sedikit kecamatan di indonesia saja, tidak semua kecamatan ada, untuk yg ke 2 widnyana putra, saya cek linknya diblokir indihome, yg ketiga itu ya sql database biasa provinsi. kabupaten & kecamatan
[1:26 PM, 3/16/2022] Fajar Sulaksono: https://github.com/bachors/apiapi#kode-pos-api
[1:26 PM, 3/16/2022] fjr-oedadesign: Ok Jar, tp yy dibutuhin cuma frontend aja, gapake backend, page 1 login dummy, page 2 lsg form, trs ada export
[1:26 PM, 3/16/2022] Fajar Sulaksono: https://dev.farizdotid.com/api/daerahindonesia/provinsi
[1:27 PM, 3/16/2022] Fajar Sulaksono: login dummy maksudnya gimana ya mas ?
[1:28 PM, 3/16/2022] Fajar Sulaksono: ini jadinya berapa page ?
[1:28 PM, 3/16/2022] Fajar Sulaksono: 3 page ? login, isi form & export ?
[1:28 PM, 3/16/2022] fjr-oedadesign: Diisi ga di isi bs lsg ganti page
[1:28 PM, 3/16/2022] fjr-oedadesign: Form klik nya bukan save, tp export
[1:29 PM, 3/16/2022] Fajar Sulaksono: isi formnya 1 data saja kemudian di export ke file ?
[1:30 PM, 3/16/2022] fjr-oedadesign: Misal di form ada 5 field, prop, kotamadya, kab, kec , kodepos. Kalo di isi jakarta aja, klik export, yg keluar semua data yg ada di jakarta.

Kalo misal abis jakarta dia pilih jakpus, export yg keluar hanya data jakpus, dst
[1:31 PM, 3/16/2022] fjr-oedadesign: Sementara br info export ke fiile umum, csv, mysql
[1:31 PM, 3/16/2022] Fajar Sulaksono: "semua data" ini maksudnya data apa ya mas ?
[1:32 PM, 3/16/2022] Fajar Sulaksono: kalau buttonnya cuman 1 pilih tipe file exportnya gimana ya mas ?
[1:32 PM, 3/16/2022] fjr-oedadesign: Data yg ada di databasenya
[1:32 PM, 3/16/2022] Fajar Sulaksono: oo datanya sudah ada ?
[1:32 PM, 3/16/2022] Fajar Sulaksono: struktur datanya boleh saya lihat ?
[1:33 PM, 3/16/2022] fjr-oedadesign: Iya td yg dr source Fajar
[1:34 PM, 3/16/2022] fjr-oedadesign: Mungkin ada beberapa button file export umum, yg sy tau baru .csv dan .db
[1:35 PM, 3/16/2022] Fajar Sulaksono: apakah maksudnya begini ada tabel : kelurahan, kecamatan, kabupaten, provinsi trus bisa dipilih sesuai pilihan formnya begitu mas ?
[1:38 PM, 3/16/2022] fjr-oedadesign: Iya jd tergantung sampai mana yg di pilih
[1:38 PM, 3/16/2022] fjr-oedadesign: Kalo dipilih prop, hasilnya pasti banyak kan? Dibanding hasil kalo dipilih kotamadya. Dst
[1:39 PM, 3/16/2022] fjr-oedadesign: Jd data hasil export tergantung sampai mana user pilih nya
[1:40 PM, 3/16/2022] fjr-oedadesign: Kalo trakhir dia pilih sampai kodepos, hasilnya cuma 1 list kodepos tsb lengkap dgn turunan diatas nya yg dipilih
[1:40 PM, 3/16/2022] Fajar Sulaksono: hasil exportnya spt apakah spt ini maksudnya mas ?
[1:40 PM, 3/16/2022] fjr-oedadesign: Iy ini kalo di pilih sampai field kodepos
[1:41 PM, 3/16/2022] fjr-oedadesign: Kalau dia pilih sampai kecamatan, hasilnya pasti ga cuma 1 kan? Semua kelurahan yg ada di kecamatan itu dimunculin
[1:41 PM, 3/16/2022] Fajar Sulaksono: tampilannya maksudnya spt ini : https://sig.bps.go.id/bridging-kode/index ?
[1:47 PM, 3/16/2022] fjr-oedadesign: Duh koneksi ilang
[1:49 PM, 3/16/2022] fjr-oedadesign: Iya benar Jar, fild nya pake auto input, misal pas ketik JA muncul pilihan JAkarta JAmbi
[1:50 PM, 3/16/2022] Fajar Sulaksono: ini pake Select2 namanya mas : https://select2.org/getting-started/basic-usage
[1:53 PM, 3/16/2022] Fajar Sulaksono: jadi sampai saat ini speknya spt ini ya :
1. Data lengkap provinsi, kabupaten/kota, kecamatan, kode-pos, seandainya tdk ada (dicari)
2. 3 halaman : 1 login dummy, 2 input-form, & 3 untuk export
3. Dropdown dinamis (dipilih provinsi jawa barat, maka turunannya kabupaten & kecamatan jawa barat semua)
4. Dropdown bisa search (Select2)
[1:53 PM, 3/16/2022] fjr-oedadesign: Ga ngerti maksudnya ini Jar
[1:54 PM, 3/16/2022] Fajar Sulaksono: maksudnya spt ini kan mas ?
[1:54 PM, 3/16/2022] fjr-oedadesign: Iya ada semua yg berawalan ala, kalo diterusin ketik alas, si alabama ga ada
[1:55 PM, 3/16/2022] Fajar Sulaksono: iya mas, untuk seperti itu perlu library js namanya Select2
[1:57 PM, 3/16/2022] Fajar Sulaksono: kemudian untuk wilayahnya ini cukup sampai kecamatan saja, atau perlu sampai kelurahan ya ?
[1:57 PM, 3/16/2022] fjr-oedadesign: Prop kota kab kec kodepos
Hasil search ada export download sesuai pilihan difile
[1:58 PM, 3/16/2022] fjr-oedadesign: Sampai 5 turunan tadi Jar
[1:58 PM, 3/16/2022] fjr-oedadesign: Eh 4 ya? Kodepos itu sama dgn kecamatan bukan?
[1:59 PM, 3/16/2022] fjr-oedadesign: Paling rendah kecamatan kan?
[1:59 PM, 3/16/2022] Fajar Sulaksono: karena begini mas, kodepos itu tidak mengikat di kecamatan, ada wilayah yg kodeposnya untuk 1 kecamatan saja, ada yg 1 kecamatan masing2 kelurahan punya kode pos sendiri
[1:59 PM, 3/16/2022] Fajar Sulaksono: misalnya daerah tempat tinggal saya kecamatan cileungsi masing2 kelurahan punya kode pos sendiri2
[1:59 PM, 3/16/2022] fjr-oedadesign: Jd kelurahan = kodepos ya?
[2:00 PM, 3/16/2022] Fajar Sulaksono: belum tentu mas, di jogjakarta/DIY, 1 kecamatan = 1 kode pos
[2:01 PM, 3/16/2022] fjr-oedadesign: Jd intinya sampai dgn 5 pilihan td Jar
[2:02 PM, 3/16/2022] Fajar Sulaksono: sampai kecamatan saja ya ?
[2:02 PM, 3/16/2022] fjr-oedadesign: Sampai kodepos kan terakhir urutan nya kodepos
[2:03 PM, 3/16/2022] fjr-oedadesign: Misal pas di pilih lengkap sampai kodepos 16821, result nya cuma 1 baris
[2:03 PM, 3/16/2022] Fajar Sulaksono: kalau sudah milih sampai kode pos berarti data yg akan tampil cuman 1 ?
[2:04 PM, 3/16/2022] fjr-oedadesign: Iya
[2:04 PM, 3/16/2022] Fajar Sulaksono: 1. Data lengkap provinsi, kabupaten/kota, kecamatan, kode-pos, seandainya tdk ada (dicari) (sampai kecamatan saja)
2. 3 halaman : 1 login dummy, 2 input-form, & 3 untuk export
3. Dropdown dinamis (dipilih provinsi jawa barat, maka turunannya kabupaten & kecamatan jawa barat semua)
4. Dropdown bisa search (Select2)
[2:04 PM, 3/16/2022] Fajar Sulaksono: dari spek ini apakah ada tambahan lagi mas ?
[2:05 PM, 3/16/2022] Fajar Sulaksono: kemudian untuk hasilnya yg diminta source code saja, atau berupa web yg urlnya bisa diakses ?
[2:06 PM, 3/16/2022] fjr-oedadesign: Sementara infonya br itu
[2:06 PM, 3/16/2022] fjr-oedadesign: Nanti yg diminta sourcecode dengan database nya
[2:07 PM, 3/16/2022] Fajar Sulaksono: tools yg dipake bebas saja terserah saya ?
[2:08 PM, 3/16/2022] fjr-oedadesign: Iya
[2:11 PM, 3/16/2022] fjr-oedadesign: Cost nya jd gmn
[2:11 PM, 3/16/2022] Fajar Sulaksono: baik ok mas costnya masih sama ya mas 500rb, dengan data kodepos yg dipake data dari BPS, sumbernya dari sini : https://github.com/edwin/database-kodepos-seluruh-indonesia
[2:12 PM, 3/16/2022] fjr-oedadesign: Kombinasi posindonesia sama bpja ya?
[2:12 PM, 3/16/2022] fjr-oedadesign: Ok mudah2an terpercaya
[2:12 PM, 3/16/2022] Fajar Sulaksono: dari dari Pos-Indonesia & BPS berbeda mas
[2:13 PM, 3/16/2022] fjr-oedadesign: Sy lsg infoin harganya full ya
[2:13 PM, 3/16/2022] Fajar Sulaksono: pertimbangan saya memakai data BPS karena versinya lebih baru
[2:13 PM, 3/16/2022] fjr-oedadesign: Noted
[2:14 PM, 3/16/2022] Fajar Sulaksono: Pos Indonesia : data agustus 2015
BPS : Desember 2021
[2:15 PM, 3/16/2022] fjr-oedadesign: Sip
[2:15 PM, 3/16/2022] Fajar Sulaksono: ya ok mas
[2:16 PM, 3/16/2022] fjr-oedadesign: Btw yg migrasi udh bs Jar?
[2:16 PM, 3/16/2022] fjr-oedadesign: Migrasi imel wihtburh
[2:16 PM, 3/16/2022] Fajar Sulaksono: belum sempat saya mas untuk testing emailnya
[7:58 PM, 3/16/2022] fjr-oedadesign: boleh tlg dicoba Jar, sebelum domain nya keburu expired
[3:28 PM, 3/21/2022] fjr-oedadesign: Jar viktoria kena hack lg, ini penjelasan hosting:

I have checked the hosting account about the issue and I would like to say that you have purchased the SiteLock Security - Essential for the domain viktoriadesigns.com.au to remove the infected contents from the website files. So the SiteLock Security - Essential will only remove the infected contents from the website files(Files under public_html directory). But I would like to say that there are infected files outside the public_html directory which will be not removed by the SiteLock Security - Essential and those infected files outside the public_html will infect the website files again and again and the website will start redirecting again.
[3:29 PM, 3/21/2022] fjr-oedadesign: Gmn cara bersihin yg diluar public_html?
[3:29 PM, 3/21/2022] fjr-oedadesign: Cost bya brp Jar utk clean dan protect web nya
[3:34 PM, 3/21/2022] Fajar Sulaksono: saya cek ada di 2 folder ini
[3:38 PM, 3/21/2022] Fajar Sulaksono: folder patior & vim ini masih bisa masuk file injectnya, tapi script nya sudah tdk bisa dijalankan lagi, karena fungsi2 nya sudah didisable di php.ini
[3:38 PM, 3/21/2022] Fajar Sulaksono: ini saya sudah lakukan proteksi standar yg dulu konfigurasi untuk disable fungsi php yg berbahaya
[3:39 PM, 3/21/2022] Fajar Sulaksono: kalau untuk proteksi standar spt disable fungsi php yg berbahaya di cpanel dari saya gratis saja mas, selebihnya kalau bersih2 gak bisa saya
[4:10 PM, 3/21/2022] fjr-oedadesign: Kalau udah di disable fungsi php nya tp masih bs kena hack, apa artinya si virusnya ada di dalem public_html?
[4:11 PM, 3/21/2022] Fajar Sulaksono: script yg sudah dimasukkan ke dalam itu sudah tdk bisa dijalankan lagi mas setahu saya, kenapa scriptnya masih bisa masuk mungkin lewat plugin wordpress jg
[4:12 PM, 3/21/2022] Fajar Sulaksono: yg di folder patior sama vim ini saya coba jalankan urlnya cuman blank putih kosong saja
[4:12 PM, 3/21/2022] Fajar Sulaksono: yg dimaksud kena hack ini, masalah persisnya spt apa mas ?
